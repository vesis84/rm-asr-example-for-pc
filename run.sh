#!/bin/bash
# Copyright 2019, Brno University of Technology (Author: Karel Vesely).

# This script will show you a generic structure of a Kaldi
# recipe for training a Speech Recognition system.
# You can find other recipes in $KALDI_ROOT/egs, to see
# that their structure is the same.
#
# The 'master' script is 'run.sh',
# - it first prepares (i.e. formats) the speech database in data/train data/test directoies,
# - then, the training of the Speech Recognition system is done.
#
# Other important scripts are 'path.sh' and 'cmd.sh',
# - 'path.sh' modifies environment variable '$PATH' to access the kaldi command line programs (from $KALDI_ROOT/src/*bin/),
# - 'cmd.sh' defines variables $train_cmd $decode_cmd, these control how the code is executed,
#    - either on local machine with 'run.pl' or in cluster-computer with 'queue.pl' (if Sun Grid Engine is available),
#
# Next, there are 3 directories with scripts:
# steps/ : parts of the Speech Reocgnition recipe (i.e. training, decoding, alignment),
# utils/ : utilities and data manipulation code that is same for all languages,
# local/ : scripts specific to only one 'recipe or language',
# - note: folders 'steps/' and 'utils/' are shared by all the recipes.
#
# And lastly, these two 'output' directories produced by this script:
# data/  : outputs of data preparation (formatted speech data, lexicon, langauge model),
# exp/   : the root directory with acoustic models, that will be in subdirectories.

# This will enable the debug mode (don't use, if you are copy-pasting to shell),
set -euxo pipefail # exit on error, enable debug mode,

# First, we do some preparations so our recipe can access kaldi:
{ KALDI_ROOT=/usr/local/share/Kaldi
  [ ! -e steps ] && ln -s $KALDI_ROOT/egs/wsj/s5/steps
  [ ! -e utils ] && ln -s $KALDI_ROOT/egs/wsj/s5/utils
  sed -i "s:KALDI_ROOT=.*:KALDI_ROOT=$KALDI_ROOT:" path.sh
}

. ./path.sh # this gives access to kaldi tools in '$PATH',
. ./cmd.sh # this defines '$train_cmd',

# format the sepeech data into standard directories: 'data/train' 'data/test_*',
local/rm_data_prep.sh $PWD/RM
# local/rm_data_prep.sh /mnt/matylda2/data/RM # @BUT,

# TASK1:
# look into 'data/train', the files and its 'line formats' are:
# - wav.scp : a 'wav-key' and a command that writes a 'wav' into pipe (or can be a wav filename),
# - utt2spk : mapping of utterances (i.e. sentences) to speakers,
# - spk2utt : speaker-key and a list of all its utterances,
# - text    : utterance-key and corresponding reference transcript,
# Alterantively, there could be 'segments' file, which allows to have more utterances in a single wav,
# its format is: <utterance-key> <wav-key> <time-begin> <time-end>.
#
# More info: http://kaldi-asr.org/doc/data_prep.html (section: Data preparation, the "data" part)
# Some notes are also in the file: local/rm_data_prep.sh, please look at them.


# Prepare 'data/lang'. This folder contains pronunciation lexicon, phoneme set and HMM topology.
utils/prepare_lang.sh --position-dependent-phones false \
  data/local/dict '!SIL' data/local/lang data/lang

# TASK2:
# look into 'data/lang', the interesting files are:
# - words.txt  : 'word' to 'int' mapping, kaldi works internally with integers, this avoids encoding problems,
# - phones.txt : 'phones' to 'int' mapping,
# - L.fst      : lexicon in openfst format,
#   - the human readable lexicon is here: data/local/dict/lexicon.txt
#   - the FST version is displayed by: '. path.sh; fstprint data/lang/L.fst | more'
# - oov.txt    : a 'placeholder' symbol for an Out Of Vocabulary word in training transcripts,
# - topo       : this defines the HMM model topology for all phonemes,
#
# More info: http://kaldi-asr.org/doc/data_prep.html (section: Contents of the "lang" directory)


# For our Speech Recognizer we also need a Language Model.
# - a language gives scores to words in a sentence,
#    - without language model, the recognizer would produce lots of short words,
# - in our RM recipe, the vocabulary is small and the allowed word sequences are generated from a fixed grammar,
#   (- in a practical system, the Language Model would be trained on a large corpus of text)
#
# Here we train a 'bigram' language model, which grasps context from previous word P(w_i | w_{i-1})
local/rm_prepare_grammar.sh      # Traditional RM grammar (bigram word-pair)
#
# We also preare a 'unigram' language model, which works with no context,
# - it is based only on relative frequencies of words P(w_i),
local/rm_prepare_grammar_ug.sh   # Unigram grammar (gives worse results, but
                                 # changes in WER will be more significant.)

# TASK3:
# - look into the script 'local/rm_prepare_grammar.sh'
# - what is the name of the '.fst' file with the grammar?


### AT THIS POINT, THE DATA PREPARATION AND FORMATTING IS FINISHED.


# We continue by computing MFCC features from the audio signal.
# We also compute the mean/variance normalization statistics (cmvn = cepstral mean and variance normalization).
#
# - the features are stored grouped in archives '*.ark' and indexed by 'feats.scp' file with offsets into .ark (for example: ":13146").
for x in test_mar87 test_oct87 test_feb89 test_oct89 test_feb91 test_sep92 train; do
  steps/make_mfcc.sh --nj 8 --cmd "$train_cmd" data/$x
  steps/compute_cmvn_stats.sh data/$x
done

# TASK4:
# look into the 'scp' file: 'less data/train/feats.scp'
# show the features: 'copy-feats scp:data/train/feats.scp ark,t:- | more'


# Merge the test sets into one bigger set. We do all our testing on
# this averaged set.  This is just less hassle.  We regenerate the CMVN
# stats as one of the speakers appears in two of the test sets;
# otherwise tools complain as the archive has 2 entries.
utils/combine_data.sh data/test data/test_{mar87,oct87,feb89,oct89,feb91,sep92}
steps/compute_cmvn_stats.sh data/test

# We make a subset of 1000 sentences for initial step of training,
utils/subset_data_dir.sh data/train 1000 data/train.1k


### LET'S TRAIN SEVERAL GMM-HMM SPEECH-TO-TEXT SYSTEMS,
# - we did not cover Gaussian Mixture Models in the slides,
#   the likelihood P(x|s) from the acoustic model is:
#
#   P(x|s) = sum_i{ w_i * N(x|mean_{i,s},variance_{i,s}) }
#
#   GMM is a weighted sum of Gaussians: N(x|mean,veriance).
#   It fits the distribution of data in the feature space.
#
#   see: https://scikit-learn.org/stable/modules/mixture.html


### MONOPHONE SYSTEM (CONTEXT INDEPENDENT PHONEMES),

# Train a monophone model (context independent phonemes, 3 states per phoneme)
steps/train_mono.sh --nj 4 --cmd "$train_cmd" data/train.1k data/lang exp/mono

# Build a recognition network 'HCLG.fst', this is built from:
# 1) acoustic model (exp/mono/final.mdl), 2) language model (data/lang/G.fst) and 3) lexicon (data/lang/L.fst).
utils/mkgraph.sh data/lang exp/mono exp/mono/graph
# You can look into 'utils/mkgraph.sh' to see how, the network is gradually built: G -> LG -> CLG -> HCLG

# Run the decoder, it uses acoustic model 'final.mdl' and recognition network 'HCLG.fst'
# to generate lattices by binary tool 'gmm-latgen-faster', then the Word Error Rate scoring
# is done by 'local/score.sh'.
#
# Note: The scoring in 'local/score.sh' is doing a 'grid search' (i.e. try all values) over a range
# of language-model werights: $min_lmwt, $max_lmwt and also searches for optimal $word_ins_penalty.
steps/decode.sh --config conf/decode.config --nj 20 --cmd "$decode_cmd" \
  exp/mono/graph data/test exp/mono/decode
# see the Word-Error-Rate by running 'bash RESULTS'

# Get alignment of training data with the monophone model.
steps/align_si.sh --nj 8 --cmd "$train_cmd" \
  data/train data/lang exp/mono exp/mono_ali


### TRIPHONE SYSTEM (CONTEXT DEPENDENT PHONEMES),
# - context dependent triphones: "b r n o" -> "<sil>-b+r b-r+n r-n+o n-o+<sil>"
# - clustering, there are 500 GMMs, in total having 3000 Gaussians.
# - input features are: MFCCs with deltas (context, encodes slope of the feature trajectory).

# train tri1 [first triphone pass]
steps/train_deltas.sh --cmd "$train_cmd" \
  500 3000 data/train data/lang exp/mono_ali exp/tri1

# decode tri1
utils/mkgraph.sh data/lang exp/tri1 exp/tri1/graph
steps/decode.sh --config conf/decode.config --nj 20 --cmd "$decode_cmd" \
  exp/tri1/graph data/test exp/tri1/decode
# see the Word-Error-Rate by running 'bash RESULTS'

# TASK5:
# look at the tree of context dependent states,
# 'draw-tree data/lang/phones.txt exp/tri1/tree | dot -Tps -Gsize=8,10.5 | ps2pdf - tree.pdf'
# 'acroread tree.pdf'
# -> look at the bottom of the blank page, zoom in,
# -> question types:
#      "Center=?" what is the center phoneme in triphone 'b-r+n' -> 'r'
#      "PdfClass=?" -> '0,1' = it is 1st or 2nd state in HMM. 'NO' = it is none of these.
#      "RContext=?" -> 'ae,ax,eh' = right context is any of these phoenemes. 'NO' = right context of none of these.
#      "LContext=?" -> 'ae,ax,eh' = left context is any of these phoenemes. 'NO' = legt context of none of these.
# -> terminal nodes identify which GMM to use for the HMM-state.

# align tri1
steps/align_si.sh --nj 8 --cmd "$train_cmd" \
  --use-graphs true data/train data/lang exp/tri1 exp/tri1_ali


### TRIPHONE SYSTEM (CONTEXT DEPENDENT PHONEMES),
# - train a bigger GMM model (1800 states, 9000 Gaussians),
# - 'lda_mllt' = there is a linear transformation (matrix) applied
#   to a window of input features, this improves WER performance.
#   ( LDA  = http://kaldi-asr.org/doc/transform.html#transform_lda )
#   ( MLLT = http://kaldi-asr.org/doc/transform.html#transform_mllt )

# train and decode tri2b [LDA+MLLT]
steps/train_lda_mllt.sh --cmd "$train_cmd" \
  --splice-opts "--left-context=3 --right-context=3" \
  1800 9000 data/train data/lang exp/tri1_ali exp/tri2b

# TASK6:
# look into the training script 'steps/train_lda_mllt.sh',
# find the main loop: 'while [ $x -lt $num_iters ]; do',
# see what binary programs are run there.
# ( gmm-acc-stats-ali, gmm-est )
#
# Look at their command line interface: run 'gmm-est' in shell without parameters.

utils/mkgraph.sh data/lang exp/tri2b exp/tri2b/graph

steps/decode.sh --config conf/decode.config --nj 20 --cmd "$decode_cmd" \
  exp/tri2b/graph data/test exp/tri2b/decode
# see the Word-Error-Rate by running 'bash RESULTS'

# Align all data with LDA+MLLT system (tri2b)
steps/align_si.sh --nj 8 --cmd "$train_cmd" --use-graphs true \
  data/train data/lang exp/tri2b exp/tri2b_ali

# TASK7:
# look at the alignemnts, use : 'steps/get_train_ctm.sh'
# The ctm format is: "<utt-key> <channel> <time_begin> <duration> <word>"
# You can open the wav in Audacity and the import labels and verify if alignment is correct by listening.

# TASK8:
# see how the word-error-rate depends on language model scale.
# parse the WERs from from exp/tri2b/decode/wer_${lmwt}_${wip} for wip=0.0, plot a graph (for example by matplotlib in python)


# chain recipe (requires GPU),
local/chain/run_tdnn.sh

# Or, to save time, you can download the pre-built experiment:
wget http://www.fit.vutbr.cz/~iveselyk/data/tallinn-chain_nnet.tar.gz
tar xvzf tallinn-chain_nnet.tar.gz
# and see results,
bash RESULTS
# and see the per-utterance word-alignment,
less exp/chain/tdnn_5o/decode/scoring_kaldi/wer_details/per_utt


